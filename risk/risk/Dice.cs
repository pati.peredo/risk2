﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class Dice
    {
        public Color color { get; set; }
        public int value { get; set; }

        public Dice()
        {
            color = Color.Black;
        }

        public void roll() {
            value = new Random().Next(1,7);
        }
    }
}
