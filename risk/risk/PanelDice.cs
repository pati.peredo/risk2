﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace risk
{
    public class PanelDice : Panel
    {
        //configuration player view
        private Label labelPlayerTitle, labelPlayerName;
        private Label labelAttack, labelGreatherAttack1, labelGreatherAttack2, labelGreatherAttackResult1, labelGreatherAttackResult2;
        private Label[] labelDiceAttack;
        private Label labelDefense, labelGreatherDefense1, labelGreatherDefense2, labelGreatherDefenseResult1, labelGreatherDefenseResult2;
        private Label[] labelDiceDefense;

        public List<DiceAttack> diceAttack { get; set; }
        public List<DiceDefense> diceDefense { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int width { get; set; }
        public int height { get; set; }

        public int TOTAL_DICE = 5;
        public int TOTAL_ATACK = 3;
        public int TOTAL_DEFENSE = 2;

        public PanelDice(int x, int y, int width, int height) : base()
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public void createComponent() {
            Location = new Point(x, y);
            Size = new Size(width, height);
            TabIndex = 0;
            createAllDice();
            addComponentDice();
        }
        public void createAllDice() {
            diceAttack = new List<DiceAttack>();
            diceDefense = new List<DiceDefense>();

            for (int i = 0; i < TOTAL_DICE; i++)
            {
                if (i < 3)
                {
                    DiceAttack attack = new DiceAttack();
                    attack.roll();
                    diceAttack.Add(attack);
                }
                else {
                    DiceDefense defense = new DiceDefense();
                    defense.roll();
                    diceDefense.Add(defense);
                }
            }
        }
        public void addComponentDice() {
            intializeAllLabels();
            positioningOfLabels();
        }

        public void intializeAllLabels() {
            labelPlayerTitle = new Label();
            labelPlayerName = new Label();
            labelAttack = new Label();
            labelGreatherAttack1 = new Label();
            labelGreatherAttack2 = new Label();
            labelGreatherAttackResult1 = new Label();
            labelGreatherAttackResult2 = new Label();
            labelDiceAttack = new Label[TOTAL_ATACK];
            for (int i = 0; i < TOTAL_ATACK; i++)
            {
                labelDiceAttack[i] = new Label();
            }

            labelDefense = new Label();
            labelGreatherDefense1 = new Label();
            labelGreatherDefense2 = new Label();
            labelGreatherDefenseResult1 = new Label();
            labelGreatherDefenseResult2 = new Label();
            labelDiceDefense = new Label[TOTAL_DEFENSE];
            for (int i = 0; i < TOTAL_DEFENSE; i++)
            {
                labelDiceDefense[i] = new Label();
            }
        }
        public void positioningOfLabels() {
            int x, y, width, height, jumpX, jumpY;

            x = 0;
            y = 0;
            width = 100;
            height = 20;

            jumpX = 10;
            jumpY = 20;

            labelPlayerTitle.Location = new Point(x, y);
            labelPlayerTitle.Size = new Size(width, height);
            labelPlayerTitle.Text = "Player turn:";


            labelPlayerName.Location = new Point(x + width - 30, y);
            labelPlayerName.Size = new Size(width, height);
            labelPlayerName.Text = "Player x";
            //attack
            labelAttack.Location = new Point(x + 20, y + jumpY);
            labelAttack.Size = new Size(width, height);
            labelAttack.Text = "Dice attack";

            labelGreatherAttack1.Location = new Point(x, y + (2 * height) + jumpY);
            labelGreatherAttack1.Size = new Size(width - 30, height);
            labelGreatherAttack1.Text = "Greather 1-A:";

            labelGreatherAttackResult1.Location = new Point(x + 70, y + (2 * height) + jumpY);
            labelGreatherAttackResult1.Size = new Size(width - 50, height);
            labelGreatherAttackResult1.Text = "V1-A";

            labelGreatherAttack2.Location = new Point(x, y + (3 * height) + jumpY);
            labelGreatherAttack2.Size = new Size(width - 30, height);
            labelGreatherAttack2.Text = "Greather 2-A:";

            labelGreatherAttackResult2.Location = new Point(x + 70, y + (3 * height) + jumpY);
            labelGreatherAttackResult2.Size = new Size(width - 50, height);
            labelGreatherAttackResult2.Text = "V2-A";

            int jumpDice = 0;

            for (int i = 0; i < diceAttack.Count; i++)
            {
                labelDiceAttack[i].Location = new Point(x + jumpDice, y + (4 * height) + jumpY);
                labelDiceAttack[i].Size = new Size(width - 70, height + 10);
                labelDiceAttack[i].BackColor = diceAttack.ElementAt(i).color;
                labelDiceAttack[i].Text = $"{ diceAttack.ElementAt(i).value }";    
                jumpDice += 40;

                Controls.Add(labelDiceAttack[i]);
            }
            //defense
            labelDefense.Location = new Point(x + 200, y + jumpY);
            labelDefense.Size = new Size(width, height);
            labelDefense.Text = "Dice Defense";

            labelGreatherDefense1.Location = new Point(x + 170, y + (2 * height) + jumpY);
            labelGreatherDefense1.Size = new Size(width - 20, height);
            labelGreatherDefense1.Text = "Greather 1-D:";

            labelGreatherDefenseResult1.Location = new Point(x + 250, y + (2 * height) + jumpY);
            labelGreatherDefenseResult1.Size = new Size(width + 20, height);
            labelGreatherDefenseResult1.Text = "V1-D";

            labelGreatherDefense2.Location = new Point(x + 170, y + (3 * height) + jumpY);
            labelGreatherDefense2.Size = new Size(width - 20, height);
            labelGreatherDefense2.Text = "Greather 2-D:";

            labelGreatherDefenseResult2.Location = new Point(x + 250, y + (3 * height) + jumpY);
            labelGreatherDefenseResult2.Size = new Size(width + 20, height);
            labelGreatherDefenseResult2.Text = "V2-D";

            jumpDice = 0;

            for (int i = 0; i < diceDefense.Count; i++)
            {
                labelDiceDefense[i].Location = new Point(200 + jumpDice, y + (4 * height) + jumpY);
                labelDiceDefense[i].Size = new Size(width - 70, height + 10);
                labelDiceDefense[i].BackColor = diceDefense.ElementAt(i).color;
                labelDiceDefense[i].Text = $"{ diceDefense.ElementAt(i).value }";
                jumpDice += 40;

                Controls.Add(labelDiceDefense[i]);
            }

            //attack
            Controls.Add(labelPlayerName);
            Controls.Add(labelAttack);
            Controls.Add(labelPlayerTitle);
            Controls.Add(labelGreatherAttack1);
            Controls.Add(labelGreatherAttack2);
            Controls.Add(labelGreatherAttackResult1);
            Controls.Add(labelGreatherAttackResult2);
            //defense
            Controls.Add(labelDefense);
            Controls.Add(labelGreatherDefense1);
            Controls.Add(labelGreatherDefenseResult1);
            Controls.Add(labelGreatherDefense2);
            Controls.Add(labelGreatherDefenseResult2);
        }
    }
}
