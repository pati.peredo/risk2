﻿using System.Collections.Generic;

namespace risk
{
    partial class Board
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private PanelTroop panelTroop = null;

        private PanelDice panelDice = null;

        private PanelPlayer panelPlayer = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Board
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "Board";
            this.Text = "Board-Risk";
            this.ResumeLayout(false);

        }

        public void initializePanelTroopComponent(List<Troop> troops) {
            //for loop if its more than one player, define x,y, and space
            this.SuspendLayout();
            panelTroop = new PanelTroop(178, 12, 200, 100);
            panelTroop.troops = troops;
            panelTroop.createComponent();
            this.Controls.Add(panelTroop);
        }
        public void InitializeComponentDice()
        {
            this.SuspendLayout();
            panelDice = new PanelDice(0, 250, 305, 225);
            panelDice.createComponent();
            this.Controls.Add(panelDice);
        }
        public void initializeComponentPlayer(List<Player> players)
        {
            this.SuspendLayout();
            panelPlayer = new PanelPlayer(680, 20, 60, 40);
            panelPlayer.players = players;
            panelPlayer.createComponent();
            this.Controls.Add(panelPlayer);
        }

        public void initializeComponentPlayerTwo(List<Player> players)
        {
            this.SuspendLayout();
            panelPlayer = new PanelPlayer(680, 350, 60, 40);
            panelPlayer.players = players;
            panelPlayer.createComponent();
            this.Controls.Add(panelPlayer);
        }
        #endregion
    }
}

