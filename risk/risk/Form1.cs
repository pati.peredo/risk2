﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace risk
{
    public partial class Board : Form
    {
        public Board()
        {
            InitializeComponent();
            List<Troop> tr = new List<Troop>() { new Infantery("Infantery", Color.Red, 100), new Caballery("Caballery", Color.Yellow, 100), new Artillery("Artillery", Color.Green, 100) };
            List<Player> pl = new List<Player>() { new Player("Player One", Color.Pink) };
            List<Player> pl2 = new List<Player>() { new Player("Player Two", Color.Turquoise)};
        initializePanelTroopComponent(tr);
            InitializeComponentDice();
            initializeComponentPlayer(pl);
            initializeComponentPlayerTwo(pl2);
        }
    }
}
