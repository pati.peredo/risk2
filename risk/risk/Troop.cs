﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class Troop
    {
        public string name { get; set; }
        public Color image { get; set; }
        public int quantity { get; set; }
        public int equivalence { get; set; }

        public Troop(string name, Color image, int quantity)
        {
            this.name = name;
            this.image = image;
            this.quantity = quantity;
            equivalence = 0;
        }
    }
}