﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace risk
{
    public class PanelPlayer : Panel
    {
        public List<Player> players { get; set; }
        public int x {get; set;}
        public int y {get; set;}
        public int widht { get; set; }
        public int height { get; set; }

        public PanelPlayer(int x, int y, int widht, int height) : base()
        {
            this.x = x;
            this.y = y;
            this.widht = widht;
            this.height = height;
        }

        public void createComponent() {
            Location = new System.Drawing.Point(x, y);
            Size = new System.Drawing.Size(widht, height);
            TabIndex = 0;
            addComponentPlayer();
        }

        public void addComponentPlayer() {
            //System.Windows.Forms.Button button1 = new System.Windows.Forms.Button();
            Label[] labelNames = new Label[players.Count];
            Label[] labelImages = new Label[players.Count];
        //    Label[] labelQuantities = new Label[players.Count];

            for (int i = 0; i < players.Count; i++)
            {
                labelNames[i] = new Label();
                labelImages[i] = new Label();
        //        labelQuantities[i] = new Label();
            }

            int x, y, sizeWidthText, sizeHeightText , sizeWidthImage, sizeHeightImage, jumpY, jumpX;
            x = 0;
            y = 0;
            sizeWidthText = 60;
            sizeHeightText = 20;
            sizeWidthImage = sizeHeightImage = 50;
            jumpY = 2;
            jumpX = 60;


            for (int i = 0; i < players.Count; i++)
            {
                labelNames[i].Location = new Point(x, y);
                labelNames[i].Size = new Size(sizeWidthText, sizeHeightText);
                labelNames[i].Text = players.ElementAt(i).name;

                labelImages[i].Location = new Point(x, jumpY);
                labelImages[i].Size = new Size(sizeWidthImage, sizeHeightImage);
                labelImages[i].BackColor = players.ElementAt(i).color;

            /*    labelQuantities[i].Location = new Point(x, sizeHeightText + sizeHeightImage + jumpY);
                labelQuantities[i].Size = new Size(sizeWidthImage, sizeHeightImage);
                labelQuantities[i].Text = $" x { players.ElementAt(i).quantity }";
                */
                Controls.Add(labelNames[i]);
                Controls.Add(labelImages[i]);
                //Controls.Add(labelQuantities[i]);

                x += jumpX;
            }
        }
    }
}
