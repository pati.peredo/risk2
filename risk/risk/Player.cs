﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class Player
    {
        public string name { get; set; }
        public Color color { get; set; }
        public Boolean turn { get; set; }

        public Player(string name, Color color)
        {
            this.name = name;
            this.color = color;
            this.turn = false;
        }
    }
}
