﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class Caballery : Troop
    {
        public Caballery(string name, Color image, int quantity): base(name, image, quantity)
        {
            this.equivalence = 5;
        }
    }
}
